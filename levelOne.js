// load state:
// 1.load all assets for provided level
// 2.show hint and loading progress

var player;
var aliens;
var bullets;
var bulletTime = 0;
var cursors;
var fireButton;
var explosions;
var starfield;
var score = 0;
var scoreString = '';
var scoreText;
var lives;
var enemyBullet;
var firingTimer = 0;
var stateText;
var livingEnemies = [];
var backgroundMusic;
var loseMusic;
var skillMusic;
var dieSound;
var monsterdieSound;
var shootSound;
var superSkill;
var player_emitter;
var monster_emitter;
var skill = false;

var monsterCollide = false;

var levelOneState = {

    create: function()
    {
       game.physics.startSystem(Phaser.Physics.ARCADE);

        //  Tht timer
        game.time.events.add(Phaser.Timer.SECOND * 10, canuseSkill, this);

        //  The audio
        backgroundMusic = game.add.audio('bgm');
        backgroundMusic.play();
        skillMusic = game.add.audio('han');
        dieSound = game.add.audio('OOF');
        monsterdieSound = game.add.audio('monsterdie');
        shootSound = game.add.audio('shoot');
        shootSound.volume = 0.3; 
        loseMusic = game.add.audio('lose');
		//  The scrolling starfield background
		starfield = game.add.tileSprite(0, 0, 800, 600, 'starfield');

		//  Our bullet group
		bullets = game.add.group();
		bullets.enableBody = true;
		bullets.physicsBodyType = Phaser.Physics.ARCADE;
		bullets.createMultiple(30, 'bullet');
		bullets.setAll('anchor.x', 0.5);
		bullets.setAll('anchor.y', 1);
		bullets.setAll('outOfBoundsKill', true);
		bullets.setAll('checkWorldBounds', true);

		// The enemy's bullets
		enemyBullets = game.add.group();
		enemyBullets.enableBody = true;
		enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
		enemyBullets.createMultiple(30, 'enemyBullet');
		enemyBullets.setAll('anchor.x', 0.5);
		enemyBullets.setAll('anchor.y', 1);
		enemyBullets.setAll('outOfBoundsKill', true);
		enemyBullets.setAll('checkWorldBounds', true);

		//  The hero!
        player = game.add.sprite(400, 500, 'ship');
        player.animations.add('go', [0,1,2], 50, true);
		player.anchor.setTo(0.5, 0.5);
        game.physics.enable(player, Phaser.Physics.ARCADE);
        
        //  emitter
        player_emitter = game.add.emitter(0, 0, 150);
        player_emitter.makeParticles('pixel');
        player_emitter.setYSpeed(-150, 150);
        player_emitter.setXSpeed(-150, 150);
        player_emitter.setScale(2, 0, 2, 0, 800);
        player_emitter.gravity = 0;

        monster_emitter = game.add.emitter(0, 0, 150);
        monster_emitter.makeParticles('pixel');
        monster_emitter.setYSpeed(-150, 150);
        monster_emitter.setXSpeed(-150, 150);
        monster_emitter.setScale(2, 0, 2, 0, 800);
        monster_emitter.gravity = 0;

		//  The baddies!
		aliens = game.add.group();
		aliens.enableBody = true;
		aliens.physicsBodyType = Phaser.Physics.ARCADE;

		createAliens();

		//  The score
		scoreString = 'Taiwan Value : ';
		scoreText = game.add.text(10, 10, scoreString + score, { font: '30px Arial', fill: '#fff' });

		//  Lives
		lives = game.add.group();
		game.add.text(game.world.width - 300, 10, 'Retards Remaining : ', { font: '29px Arial', fill: '#fff' });

		//  Text
		stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '84px Arial', fill: '#fff' });
		stateText.anchor.setTo(0.5, 0.5);
		stateText.visible = false;

		for (var i = 0; i < 3; i++) 
		{
			var ship = lives.create(game.world.width - 100 + (40 * i), 60, 'ship');
			ship.anchor.setTo(0.5, 0.5);
			ship.angle = 0;
			ship.alpha = 0.4;
		}

		//  An explosion pool
		explosions = game.add.group();
		explosions.createMultiple(30, 'kaboom');
		explosions.forEach(setupInvader, this);

		//  And some controls to play the game with
		cursors = game.input.keyboard.createCursorKeys();
        fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        superSkill = game.input.keyboard.addKey(Phaser.Keyboard.S);
        
    },

    update: function()
    {
        starfield.tilePosition.y += 2;
        player.animations.play('go');

    if (player.alive)
    {
        //  Reset the player, then check for movement keys
        player.body.velocity.setTo(0, 0);

        if (cursors.left.isDown)
        {
            player.body.velocity.x = -200;
        }
        else if (cursors.right.isDown)
        {
            player.body.velocity.x = 200;
        }

        //  Firing?
        if (fireButton.isDown)
        {
            fireBullet();
        }

        if (superSkill.isDown && skill)
        {
            skillMusic.volume = 0.3
            skillMusic.play();
            aliens.removeAll();
            var alien = aliens.create(48, 50, 'invader');
            alien.anchor.setTo(0.5, 0.5);
            alien.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
            alien.play('fly');
            alien.body.moves = false;
        }

        if (game.time.now > firingTimer)
        {
            enemyFires();
        }

        

        //  Run collision
        game.physics.arcade.overlap(bullets, aliens, collisionHandler, null, this);
        game.physics.arcade.overlap(enemyBullets, player, enemyHitsPlayer, null, this);
    }
        
    }

};

function collisionHandler (bullet, alien) {

    //  When a bullet hits an alien we kill them both
    monsterdieSound.play();
    bullet.kill();
    alien.kill();
    monster_emitter.x = bullet.body.x;
    monster_emitter.y = bullet.body.y;
    monster_emitter.start(true, 800, null, 1500);

    //  Increase the score
    score += 20;
    scoreText.text = scoreString + score;

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(alien.body.x, alien.body.y);
    explosion.play('kaboom', 30, false, true);

    if (aliens.countLiving() == 0)
    {
        backgroundMusic.pause();
        score += 1000;
        scoreText.text = scoreString + score;

        enemyBullets.callAll('kill',this);
        stateText.text = " You so GOOD huh?";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(nextlevel,this);
    }

}

function enemyHitsPlayer (player,bullet) {
    
    bullet.kill();
    dieSound.play();
    game.camera.shake(0.01, 300, false, Phaser.Camera.SHAKE_VERTICAL);

    player_emitter.x = player.body.x;
    player_emitter.y = player.body.y;
    player_emitter.start(true, 800, null, 1500);

    live = lives.getFirstAlive();

    if (live)
    {
        live.kill();
    }

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(player.body.x, player.body.y);
    explosion.play('kaboom', 30, false, true);

    // When the player dies
    if (lives.countLiving() < 1)
    {
        backgroundMusic.pause();
        loseMusic.play();
        player.kill();
        enemyBullets.callAll('kill');

        stateText.text=" NOOB \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);
    }

}

function canuseSkill () {
    skill = true;
}

function setupInvader (invader) {

    invader.anchor.x = 0.5;
    invader.anchor.y = 0.5;
    invader.animations.add('kaboom');

}

function descend() {

    aliens.y += 10;

}

function createAliens () {

    for (var y = 0; y < 4; y++)
    {
        for (var x = 0; x < 10; x++)
        {
            var alien = aliens.create(x * 48, y * 50, 'invader');
            alien.anchor.setTo(0.5, 0.5);
            alien.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
            alien.play('fly');
            alien.body.moves = false;
        }
    }

    aliens.x = 100;
    aliens.y = 50;

    //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
    var tween = game.add.tween(aliens).to( { x: 200 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);

    //  When the tween loops it calls descend
    tween.onLoop.add(descend, this);
}

function nextlevel(){
	game.state.start('levelTwo');
}

function resetBullet (bullet) {

    //  Called if the bullet goes out of the screen
    bullet.kill();

}

function restart () {

    //  A new level starts
    
    //resets the life count
    lives.callAll('revive');
    //  And brings the aliens back from the dead :)
    aliens.removeAll();
    createAliens();

    //revives the player
    player.revive();
    //hides the text
    stateText.visible = false;

}

function enemyFires () {

    //  Grab the first bullet we can from the pool
    enemyBullet = enemyBullets.getFirstExists(false);

    livingEnemies.length=0;

    aliens.forEachAlive(function(alien){

        // put every living enemy in an array
        livingEnemies.push(alien);
    });


    if (enemyBullet && livingEnemies.length > 0)
    {
        
        var random=game.rnd.integerInRange(0,livingEnemies.length-1);

        // randomly select one of them
        var shooter=livingEnemies[random];
        // And fire the bullet from this enemy
        enemyBullet.reset(shooter.body.x, shooter.body.y);

        game.physics.arcade.moveToObject(enemyBullet,player,120);
        firingTimer = game.time.now + 1000;
    }

}

function fireBullet () {

    //  To avoid them being allowed to fire too fast we set a time limit
    if (game.time.now > bulletTime)
    {
        //  Grab the first bullet we can from the pool
        bullet = bullets.getFirstExists(false);

        if (bullet)
        {
            //  And fire it
            shootSound.play();
            bullet.reset(player.x, player.y + 8);
            bullet.body.velocity.y = -400;
            bulletTime = game.time.now + 200;
        }
    }

}