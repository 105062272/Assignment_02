# Software Studio 2019 Spring Assignment 2

# Game Story
1. 賣菜~~台~~郎要帶領台灣邁向兩岸一家親<br>
   『立足台灣、胸懷大陸、放眼世界、征服宇宙』<br>
   究竟韓國瑜能不能征服宇宙呢?<br>
   面對南北~~高麗~~菜蟲、開房堅的淫威<br>
   韓國瑜會怎麼面對呢<br>
   究竟是草包還是薩諾斯，讓我們繼續看下去<br>


## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|N|

## Website Detail Description
音樂來源:<br>
    ● 大絕招音效: <br>
        韓國瑜–請經發局伏局長回答<br>
    ● 遊戲結束音效: <br>
        The Tokens - The Lion Sleeps Tonight<br>
    ● 背景音樂:<br>
        Level 1 : Minecraft Theme Song<br>
        Level 2 : Koxx - A Fool Moon Night<br>
## 在第一關等10秒蓄力後按 "S" 發動大絕招

# Basic Components Description : 
1. Jucify mechanisms : <br>
    ● 共有兩關 <br>
    ● 大絕招 (在第一關等10秒蓄力後按 "S" 發動)<br>
2. Animations : <br>
    ● 國瑜抖動<br>
    ● 怪物抖動<br>
3. Particle Systems :<br>
    ● 敵人爆炸粒子<br>
    ● 玩家爆炸粒子<br>
4. Sound effects : <br>
    ● 敵人死亡音效<br>
    ● 玩家死亡音效<br>
    ● 大絕招音效<br>
    ● 遊戲結束音效<br>
    ● 背景音樂<br>
    ● 玩家發射子彈音效<br>
5. Leaderboard : <br>
    ● 沒時間做QQ<br>

# Bonus Functions Description : 
1. ~~第二關在音樂結束後會有驚人的事情發生~~
