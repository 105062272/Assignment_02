
var choice = "start";
var startLabel;
var menuState =
{
    create:function()
    {
        //add background
        //game.add.image(0 , 0  , 'background');
        game.stage.backgroundColor = "rgb(0,0,0)";
        var gameName = game.add.text(game.width/2 , 200 , 'Kaohsiung Fa Da Cai' , { font: '40px Eras_Demi', fill: '#bdbdbd' });
        gameName.anchor.setTo(0.5 , 0.5);
        //create some option
        startLabel = game.add.text(game.width/2 , 400 , 'start' , { font: '40px Eras_Demi', fill: '#bdbdbd' });
        startLabel.anchor.setTo(0.5 , 0.5);
        
    },
    update:function()
    {
        this.choose();
    },

    //choose one option
    choose:function()
    {
        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        if(enterKey.isDown)
        {
            if(choice == "start")
            game.state.start('levelOne');    // 1 specify which level
        }
    },
};