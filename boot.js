// boot state:
// 1.preload the menu/loading assets
// 2.load custom fonts
// 3.start loadState
var bootState = {

    preload: function()
    {
        
        game.load.image('bullet', 'assets/bullet.png');
		game.load.image('enemyBullet', 'assets/enemy-bullet.png');
        game.load.spritesheet('invader', 'assets/invader32x32x4.png', 32, 32);
        game.load.spritesheet('invader_lv2', 'assets/ciage.png', 30, 35);
        game.load.spritesheet('ship', 'assets/player30x38.png', 30, 38);
		game.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);
		game.load.image('starfield', 'assets/starfield.png');
        game.load.image('background', 'assets/background2.png');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.audio('music_lv2' ,'assets/Fool_Moon.mp3' );
        game.load.audio('shoot' ,'assets/AAA.mp3' );
        game.load.audio('OOF' ,'assets/OOF.mp3' );
        game.load.audio('han' ,'assets/han.mp3' );
        game.load.audio('monsterdie' ,'assets/bomb.mp3' );
        game.load.audio('lose' ,'assets/Lion_Sleeps.mp3' );
        game.load.audio('bgm' ,'assets/Minecraft.mp3' );
        // force loading fonts
        game.add.text(0, 0, "Hello, world", { 
            font: '1px Eras_Demi',
            fill: '#000000'
        });

        game.add.text(0, 0, "Hello, world", { 
            font: '1px Sim_Sun',
            fill: '#000000'
        });

    },

    create: function()
    {
        game.state.start('menu');

    }

};