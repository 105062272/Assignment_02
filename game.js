// Create game instance
var game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas');

// Other global variables
/* nothing here now */

// Add all states, run boot
game.state.add('boot', bootState);
game.state.add('menu', menuState);
game.state.add('levelOne', levelOneState);
game.state.add('levelTwo', levelTwoState);
game.state.start('boot');